CREATE TABLE IF NOT EXISTS product_tb (
                            product_id serial unique,
                            product_name varchar(255) NOT NULL,
                            product_price float4
);
CREATE TABLE IF NOT EXISTS customer_tb (
                             customer_id serial unique,
                             customer_name varchar(255) NOT NULL,
                             customer_address varchar(255) NOT NULL,
                             customer_phone varchar(255) NOT NULL
);
CREATE TABLE IF NOT EXISTS invoice_tb (
                            invoice_id serial unique,
                            invoice_date date NOT NULL,
                            customer_id int,
                            FOREIGN KEY (customer_id) REFERENCES customer_tb(customer_id)
);

CREATE TABLE IF NOT EXISTS invoice_detail_tb (
                                   id serial,
                                   invoice_id int,
                                   product_id int,
                                   foreign key (invoice_id) REFERENCES invoice_tb(invoice_id) ,
                                   foreign key (product_id) REFERENCES product_tb(product_id)
)