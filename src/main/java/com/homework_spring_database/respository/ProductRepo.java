package com.homework_spring_database.respository;
import com.homework_spring_database.model.CustomerModel;
import com.homework_spring_database.model.ProductModel;
import com.homework_spring_database.response.ProductResponse;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepo {
    @Select("""
            Select * From product_tb
            """)
    @Results(id="product",value={
            @Result(property="productId",column = "product_id"),
            @Result(property="productName",column = "product_name"),
            @Result(property="productPrice",column = "product_price"),
    })
    List<ProductModel> getAllProduct();
    @Select("""
            Insert into product_tb values(default,#{productName},#{productPrice}) returning *
            """)
    Integer addProduct(ProductModel productModel);
    @Update("""
            Update product_tb set product_name=#{productModel.productName},product_price=#{productModel.productPrice} where product_id = #{id}
            """)
    void updateProduct(Integer id,ProductModel productModel);
    @Delete("""
            Delete From product_tb  where product_id = #{id}
            """)
    void deleteProduct(Integer id);
    @Select("""
            Select * From product_tb where product_id = #{id}
            """)
    @ResultMap("product")
    ProductModel getProductByID(Integer id);
}
