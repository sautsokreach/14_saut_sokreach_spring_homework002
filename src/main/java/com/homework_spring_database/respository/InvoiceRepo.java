package com.homework_spring_database.respository;
import com.homework_spring_database.model.InvoiceModel;
import com.homework_spring_database.model.ProductModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepo {
    @Select("""
            Select * From invoice_tb
            """)
    @Results(id="invoice",value={
            @Result(property="invoiceId",column = "invoice_id"),
            @Result(property="invoiceDate",column = "invoice_date"),
            @Result(property="customerId",column = "customer_id"),
    })
    List<InvoiceModel> getAllInvoice();
    @Select("""
            Select product_id From invoice_detail_tb where invoice_id = #{id}
            """)
    List<Integer> getInvoiceIdDetail(Integer id);
    @Select("""
            Insert into invoice_tb values(default,#{invoiceDate},#{customerId}) returning invoice_id
            """)
    Integer addInvoice(InvoiceModel invoiceModel);
    @Select("""
            Insert into invoice_detail_tb values(default,#{id},#{productId}) returning id
            """)
    Integer addInvoiceDetail(Integer id, Integer productId);
    @Update("""
            Update invoice_tb set invoice_date=#{invoiceModel.invoiceDate},customer_id=#{invoiceModel.customerId} where invoice_id = #{id};
            """)
    void updateInvoice(Integer id,InvoiceModel invoiceModel);
    @Update("""
            Update invoice_tb set invoice_date=#{invoiceModel.invoiceDate},customer_id=#{invoiceModel.customerId} where invoice_id = #{id};
            """)
    void updateInvoiceDetail(Integer id,List<Integer> list);

    @Delete("""
            Delete From invoice_detail_tb  where invoice_id = #{id};
            Delete From invoice_tb  where invoice_id = #{id};
            """)

    void deleteInvoice(Integer id);
    @Delete("""
            Delete From invoice_detail_tb  where invoice_id = #{id};
            """)

    void deleteInvoiceDetail(Integer id);
    @Select("""
            Select * From invoice_tb where invoice_id = #{id}
            """)
    @ResultMap("invoice")
    InvoiceModel getInvoiceByID(Integer id);
}
