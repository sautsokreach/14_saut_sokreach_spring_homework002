package com.homework_spring_database.respository;
import com.homework_spring_database.model.CustomerModel;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface CustomerRepo {
    @Select("""
            Select * From customer_tb
            """)
    @Results(id="customer",value={
            @Result(property="customerId",column = "customer_id"),
            @Result(property="customerName",column = "customer_name"),
            @Result(property="customerAddress",column = "customer_address"),
            @Result(property="customerPhone",column = "customer_phone"),
    })
    List<CustomerModel> getAllCustomer();
    @Select("""
            Insert into customer_tb values(default,#{customerName},#{customerPhone},#{customerAddress}) returning *
            """)
    Integer addCustomer(CustomerModel customerModel);
    @Update("""
            Update customer_tb set customer_name=#{customerModel.customerName},customer_phone=#{customerModel.customerPhone},customer_address = #{customerModel.customerAddress} where customer_id = #{id}
            """)
    void updateCustomer(Integer id,CustomerModel customerModel);
    @Delete("""
            Delete From customer_tb  where customer_id = #{id}
            """)
    void deleteCustomer(Integer id);
    @Select("""
            Select * From customer_tb where customer_id = #{id}
            """)
    @ResultMap("customer")
    CustomerModel getCustomerByID(Integer id);
}
