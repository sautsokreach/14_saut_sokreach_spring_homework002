package com.homework_spring_database;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeworkSpringDatabaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomeworkSpringDatabaseApplication.class, args);
    }

}
