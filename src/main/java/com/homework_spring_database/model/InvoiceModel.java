package com.homework_spring_database.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public class InvoiceModel {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer invoiceId;
    private java.sql.Date invoiceDate = new java.sql.Date(new Date().getTime());
    private Integer customerId;
    private List<Integer> products;

}
