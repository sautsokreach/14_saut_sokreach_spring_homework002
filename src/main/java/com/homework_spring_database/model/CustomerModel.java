package com.homework_spring_database.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public class CustomerModel {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer customerId;
    private String customerName;
    private String customerAddress;
    private String customerPhone;

}
