package com.homework_spring_database.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public class ProductModel {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer productId;
    private String productName;
    private float productPrice;

}
