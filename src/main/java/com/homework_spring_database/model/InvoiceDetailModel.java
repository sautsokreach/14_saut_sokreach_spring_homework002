package com.homework_spring_database.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceDetailModel {

    private Integer invoiceId;
    private java.sql.Date invoiceDate = new java.sql.Date(new Date().getTime());
    private CustomerModel customer;
    private List<ProductModel> Products;

}
