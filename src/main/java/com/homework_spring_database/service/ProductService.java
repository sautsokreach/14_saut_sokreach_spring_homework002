package com.homework_spring_database.service;

import com.homework_spring_database.model.ProductModel;
import com.homework_spring_database.response.ProductResponse;

import java.util.List;

public interface ProductService {
    List<ProductModel> getAllProduct();
    ProductModel getProductByID(Integer id);
    List<ProductModel> getProductByInvoiceId(List<Integer> listProducts);
    Integer addProduct(ProductModel customerModel);
    void updateProduct(Integer id,ProductModel productModel);
    void deleteProduct(Integer id);
}
