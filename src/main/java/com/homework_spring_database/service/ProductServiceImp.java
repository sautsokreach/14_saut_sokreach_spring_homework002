package com.homework_spring_database.service;

import com.homework_spring_database.model.ProductModel;
import com.homework_spring_database.response.ProductResponse;
import com.homework_spring_database.respository.ProductRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepo productRepo;

    public ProductServiceImp(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }
    @Override
    public List<ProductModel> getAllProduct() {
        return productRepo.getAllProduct();
    }

    @Override
    public ProductModel getProductByID(Integer id) {
        return productRepo.getProductByID(id);
    }

    @Override
    public List<ProductModel> getProductByInvoiceId(List<Integer> list) {
        List<ProductModel> allProducts = new ArrayList<>();
        for (Integer i:list) {
            allProducts.add(productRepo.getProductByID(i));
        }
        return allProducts;
    }


    @Override
    public Integer addProduct(ProductModel customerModel) {
        return productRepo.addProduct(customerModel);

    }

    @Override
    public void updateProduct(Integer id,ProductModel customerModel) {
        productRepo.updateProduct(id,customerModel);
    }

    @Override
    public void deleteProduct(Integer id) {
        productRepo.deleteProduct(id);
    }
}
