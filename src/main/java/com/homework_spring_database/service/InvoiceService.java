package com.homework_spring_database.service;


import com.homework_spring_database.model.InvoiceDetailModel;
import com.homework_spring_database.model.InvoiceModel;

import java.util.List;

public interface InvoiceService {
    List<InvoiceModel> getAllInvoice();

    InvoiceDetailModel getInvoiceByID(Integer id);


    Integer addInvoice(InvoiceModel invoiceModel);
    void updateInvoice(Integer id, InvoiceModel invoiceModel);
    void deleteInvoice(Integer id);
}
