package com.homework_spring_database.service;

import com.homework_spring_database.model.CustomerModel;

import java.util.List;

public interface CustomerService {
    List<CustomerModel> getAllCustomer();
    CustomerModel getCustomerByID(Integer id);
    Integer addCustomer(CustomerModel customerModel);
    void updateCustomer(Integer id,CustomerModel customerModel);
    void deleteCustomer(Integer id);
}
