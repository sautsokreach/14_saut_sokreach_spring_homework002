package com.homework_spring_database.service;
import com.homework_spring_database.model.InvoiceDetailModel;
import com.homework_spring_database.model.InvoiceModel;
import com.homework_spring_database.respository.InvoiceRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InvoiceServiceImp implements InvoiceService {
    private final InvoiceRepo invoiceRepo;
    private InvoiceDetailModel invoiceDetailModel;
    private final CustomerService customerService;
    private final ProductService productService;
    public InvoiceServiceImp(InvoiceRepo customerRepo, CustomerService customerService, ProductService productService) {
        this.invoiceRepo = customerRepo;
        this.customerService = customerService;
        this.productService = productService;
    }
    @Override
    public List getAllInvoice() {
        List<InvoiceDetailModel> allData = new ArrayList<>();
        for (InvoiceModel i:invoiceRepo.getAllInvoice()) {
            this.invoiceDetailModel = new InvoiceDetailModel();
            invoiceDetailModel.setInvoiceId(i.getInvoiceId());
            invoiceDetailModel.setInvoiceDate(i.getInvoiceDate());
            invoiceDetailModel.setCustomer(customerService.getCustomerByID(i.getCustomerId()));
            invoiceDetailModel.setProducts(productService.getProductByInvoiceId(invoiceRepo.getInvoiceIdDetail(i.getInvoiceId())));
            allData.add(invoiceDetailModel);
        }
        return allData;
    }

    @Override
    public InvoiceDetailModel getInvoiceByID(Integer id)
    {
        this.invoiceDetailModel = new InvoiceDetailModel();
        InvoiceModel invoiceModel= invoiceRepo.getInvoiceByID(id);
        invoiceDetailModel.setInvoiceId(invoiceModel.getInvoiceId());
        invoiceDetailModel.setInvoiceDate(invoiceModel.getInvoiceDate());
        invoiceDetailModel.setCustomer(customerService.getCustomerByID(invoiceModel.getCustomerId()));
        invoiceDetailModel.setProducts(productService.getProductByInvoiceId(invoiceRepo.getInvoiceIdDetail(invoiceModel.getInvoiceId())));
        return invoiceDetailModel;
    }

    @Override
    public Integer addInvoice(InvoiceModel invoiceModel) {
        Integer insertId = invoiceRepo.addInvoice(invoiceModel);
        for (Integer i:invoiceModel.getProducts()) {
            invoiceRepo.addInvoiceDetail(insertId,i);
        }
        return insertId;
    }

    @Override
    public void updateInvoice(Integer id,InvoiceModel invoiceModel) {
        invoiceRepo.deleteInvoiceDetail(id);
        for (Integer i:invoiceModel.getProducts()) {
            invoiceRepo.addInvoiceDetail(id,i);
        }
        invoiceRepo.updateInvoice(id,invoiceModel);
    }

    @Override
    public void deleteInvoice(Integer id) {
        invoiceRepo.deleteInvoice(id);
    }
}
