package com.homework_spring_database.service;

import com.homework_spring_database.model.CustomerModel;
import com.homework_spring_database.respository.CustomerRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {
    private final CustomerRepo customerRepo;

    public CustomerServiceImp(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }
    @Override
    public List<CustomerModel> getAllCustomer() {
        return customerRepo.getAllCustomer();
    }

    @Override
    public CustomerModel getCustomerByID(Integer id) {
        return customerRepo.getCustomerByID(id);
    }

    @Override
    public Integer addCustomer(CustomerModel customerModel) {
        return customerRepo.addCustomer(customerModel);

    }

    @Override
    public void updateCustomer(Integer id,CustomerModel customerModel) {
        customerRepo.updateCustomer(id,customerModel);
    }

    @Override
    public void deleteCustomer(Integer id) {
        customerRepo.deleteCustomer(id);
    }
}
