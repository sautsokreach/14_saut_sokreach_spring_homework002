package com.homework_spring_database.controller;
import com.homework_spring_database.model.CustomerModel;
import com.homework_spring_database.response.Response;
import com.homework_spring_database.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class CustomerController {
    private final CustomerService customerService;


    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }
    @GetMapping("/api/v1/customer/get-all-customer")
    Response<?> getAllCustomer(){
        return new Response<List>(HttpStatus.OK,"Get All Customer", customerService.getAllCustomer());
    }
    @GetMapping("/api/v1/customer/get-customer-by-id/{id}")
    Response<?> getCustomerByID(@PathVariable Integer id){
        if(customerService.getCustomerByID(id) !=null){
            return new Response<>(HttpStatus.OK,"Get Customer", customerService.getCustomerByID(id));
        }
        return new Response<>(HttpStatus.OK,"Incorrect ID", customerService.getCustomerByID(id));
    }
    @PostMapping("/api/v1/customer/add-new-customer")
    public Response<?> addCustomer(@RequestBody CustomerModel customerModel){
        customerModel.setCustomerId(customerService.addCustomer(customerModel));
        return new Response<>(HttpStatus.OK,"Add Successfully", customerModel);
    }
    @PutMapping ("/api/v1/customer/update-customer-by-id/{id}")
    public Response<?> updateCustomer(@RequestBody CustomerModel customerModel, @PathVariable Integer id){
        if(customerService.getCustomerByID(id) !=null){
            customerService.updateCustomer(id,customerModel);
            customerModel.setCustomerId(id);
            return new Response<>(HttpStatus.OK,"Update Successfully", customerModel);
        }
        return new Response<>(HttpStatus.OK,"Incorrect ID", null);
    }
    @DeleteMapping  ("/api/v1/customer/delete-customer-by-id/{id}")
    public Response<?> deleteCustomer(@PathVariable Integer id){
        if(customerService.getCustomerByID(id) !=null){
        customerService.deleteCustomer(id);
            return new Response<>(HttpStatus.OK,"Delete Successfully",null);
        }
        return new Response<>(HttpStatus.OK,"Incorrect ID",null);
    }

}
