package com.homework_spring_database.controller;

import com.homework_spring_database.model.InvoiceModel;
import com.homework_spring_database.model.ProductModel;
import com.homework_spring_database.response.Response;
import com.homework_spring_database.service.InvoiceService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }
    @GetMapping("/api/v1/invoice/get-all-invoice")
    Response<?> getAllInvoice(){
        return new Response<List>(HttpStatus.OK,"Get All Invoice", invoiceService.getAllInvoice());
    }
    @GetMapping("/api/v1/invoice/get-invoice-by-id/{id}")
    Response<?> getInvoiceByID(@PathVariable Integer id){
        if(invoiceService.getInvoiceByID(id) !=null){
            return new Response<>(HttpStatus.OK,"Get invoice", invoiceService.getInvoiceByID(id));
        }
        return new Response<>(HttpStatus.OK,"Incorrect ID", invoiceService.getInvoiceByID(id));
    }
    @PostMapping("/api/v1/invoice/add-new-invoice")
    public Response<?> addInvoice(@RequestBody InvoiceModel invoiceModel){
        invoiceModel.setInvoiceId(invoiceService.addInvoice(invoiceModel));

        return new Response<>(HttpStatus.OK,"Add Successfully", invoiceModel);
    }
    @PutMapping ("/api/v1/invoice/update-invoice-by-id/{id}")
    public Response<?> updateInvoice(@RequestBody InvoiceModel invoiceModel, @PathVariable Integer id){
        if(invoiceService.getInvoiceByID(id) !=null){
            invoiceService.updateInvoice(id,invoiceModel);
            invoiceModel.setInvoiceId(id);
            return new Response<>(HttpStatus.OK,"Update Successfully", invoiceModel);
        }
        return new Response<>(HttpStatus.OK,"Incorrect ID", null);
    }
    @DeleteMapping  ("/api/v1/invoice/delete-invoice-by-id/{id}")
    public Response<?> deleteInvoice(@PathVariable Integer id){
        if(invoiceService.getInvoiceByID(id) !=null){
            invoiceService.deleteInvoice(id);
            return new Response<>(HttpStatus.OK,"Delete Successfully",null);
        }
        return new Response<>(HttpStatus.OK,"Incorrect ID",null);
    }

}
