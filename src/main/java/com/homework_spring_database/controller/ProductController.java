package com.homework_spring_database.controller;
import com.homework_spring_database.model.ProductModel;
import com.homework_spring_database.response.Response;
import com.homework_spring_database.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class ProductController {
    private final ProductService productService;


    public ProductController(ProductService customerService) {
        this.productService = customerService;
    }
    @GetMapping("/api/v1/product/get-all-product")
    Response<?> getAllProduct(){
        return new Response<List>(HttpStatus.OK,"Get All Product", productService.getAllProduct());
    }
    @GetMapping("/api/v1/product/get-product-by-id/{id}")
    Response<?> getProductByID(@PathVariable Integer id){
        if(productService.getProductByID(id) !=null){
            return new Response<>(HttpStatus.OK,"Get Product", productService.getProductByID(id));
        }
        return new Response<>(HttpStatus.OK,"Incorrect ID", productService.getProductByID(id));
    }
    @PostMapping("/api/v1/product/add-new-product")
    public Response<?> addProduct(@RequestBody ProductModel productModel){
        productModel.setProductId(productService.addProduct(productModel));
        return new Response<>(HttpStatus.OK,"Add Successfully", productModel);
    }
    @PutMapping ("/api/v1/product/update-product-by-id/{id}")
    public Response<?> updateProduct(@RequestBody ProductModel customerModel, @PathVariable Integer id){
        if(productService.getProductByID(id) !=null){
            productService.updateProduct(id,customerModel);
            customerModel.setProductId(id);
            return new Response<>(HttpStatus.OK,"Update Successfully", customerModel);
        }
        return new Response<>(HttpStatus.OK,"Incorrect ID", null);
    }
    @DeleteMapping  ("/api/v1/product/delete-product-by-id/{id}")
    public Response<?> deleteProduct(@PathVariable Integer id){
        if(productService.getProductByID(id) !=null){
            productService.deleteProduct(id);
            return new Response<>(HttpStatus.OK,"Delete Successfully",null);
        }
        return new Response<>(HttpStatus.OK,"Incorrect ID",null);
    }

}
